package SingletonPattern;

public class DbSingleton {
	
	//private instance 
	//private static DbSingleton instance = new DbSingleton();

	private static volatile DbSingleton instance = null;
	//for lazy loading 
	//	private static DbSingleton instance;
	//private constructor 
	private DbSingleton() {
		if(instance != null) {
			throw new  RuntimeException("Use getInstance() method to create");
		}
	}

	//a public method to access this class
	
	public static DbSingleton getInstance() {
		if(instance == null) {
			synchronized(DbSingleton.class) {
				if(instance == null) {
					instance = new DbSingleton();
				}
			}
		}
		return instance;
	}
}
