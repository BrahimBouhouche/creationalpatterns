package SingletonPattern;

public class DbSingletonTest {

	public static void main(String[] args) {
		//create and instance o DbSingleton
		
		DbSingleton instance = DbSingleton.getInstance();
		DbSingleton instance1 = DbSingleton.getInstance();
		DbSingleton instance2 = DbSingleton.getInstance();

		
		System.out.println(instance);
		System.out.println(instance1);
		System.out.println(instance2);

	}
	

}
